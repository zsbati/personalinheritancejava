import java.util.*;

class Person {
	protected String firstName;
	protected String lastName;
	protected int idNumber;
	
	// Constructor
	Person(String firstName, String lastName, int identification){
		this.firstName = firstName;
		this.lastName = lastName;
		this.idNumber = identification;
	}
	
	// Print person data
	public void printPerson(){
		 System.out.println(
				"Name: " + lastName + ", " + firstName 
			+ 	"\nID: " + idNumber); 
	}
	 
}

class Student extends Person{

  /*	
    *   Class Constructor
    *   
    *   @param firstName - A string denoting the Person's first name.
    *   @param lastName - A string denoting the Person's last name.
    *   @param id - An integer denoting the Person's ID number.
    *   @param scores - An array of integers denoting the Person's test scores.
    */
    // Write your constructor here

	private int[] testScores;
  Student(String firstName, String lastName, int identification, int[] testScores){
		super(firstName, lastName, identification);
    this.testScores = testScores;
    }  

    
    /*	
    *   Method Name: calculate
    *   @return A character denoting the grade.
    */
    // Write your method here
    char calculate(){
      int sum = 0;
      double avg = 0.0;
      char grade = '0';
      for (int i=0; i<testScores.length; i++){
        sum += testScores[i];
      }
      avg = sum/testScores.length;
      if (avg<= 100 && avg>= 90){
          grade = 'O'; 
          }
      if (avg<90 && avg>=80){
          grade = 'E';
          }
      if (avg<80 && avg>= 70) {
        grade = 'A';
      }   
      if (avg<70 && avg>=55){
        grade = 'P';
      }
      if (avg<55 && avg>=40){
        grade = 'D';
      }
      if (avg<40){
        grade = 'T';
      }
    
      return grade;
      }
    }


class Main {
	public static void main(String[] args) {
		Scanner scan = new Scanner(System.in);
		String firstName = scan.next();
		String lastName = scan.next();
		int id = scan.nextInt();
		int numScores = scan.nextInt();
		int[] testScores = new int[numScores];
		for(int i = 0; i < numScores; i++){
			testScores[i] = scan.nextInt();
		}
		scan.close();
		
		Student s = new Student(firstName, lastName, id, testScores);
		s.printPerson();
		System.out.println("Grade: " + s.calculate());
	}
}
